package co.csts;

public class Main {
  public static void main(String[] args) {
    String dogName = "fido";
    Dog d = new Dog(dogName);

    // read up on hash codes
    System.out.println("original dog's hashcode is: " + d.hashCode());
    System.out.println("original dog's name is: " + d.getName());

    String xml = DogXmlSerializer.serializeDog(d);

    System.out.println("\nxml looks like: " + xml + "\n");

    Dog copy = DogXmlSerializer.deserializeDog(xml);

    System.out.println("original dog's hashcode is: " + copy.hashCode());
    System.out.println("copy dog's name is: " + d.getName());

    System.out.println("\noriginal and copy have the same name?: " + d.getName().equals(copy.getName()));
    System.out.println("original and copy are the same instance?: " + d.equals(copy));
  }
}
