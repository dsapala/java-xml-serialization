package co.csts;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

public final class DogXmlSerializer {
  private DogXmlSerializer() { }

  public static String serializeDog(Dog dog) {
    String xml = "";

    try {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

      Document doc = dBuilder.newDocument();
      Element rootElement = doc.createElement("dog");
      rootElement.setAttribute("name", dog.getName());
      doc.appendChild(rootElement);

      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer();
      DOMSource source = new DOMSource(doc);
      StringWriter writer = new StringWriter();
      StreamResult result = new StreamResult(writer);

      transformer.transform(source, result);

      xml = writer.toString();
    } catch (ParserConfigurationException pce) {
      pce.printStackTrace();
    } catch (TransformerException tfe) {
      tfe.printStackTrace();
    }

    return xml;
  }

  public static Dog deserializeDog(String dogXml) {
    Dog d = new Dog();

    try {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(dogXml.getBytes("utf-8"))));
      doc.getDocumentElement().normalize();

      NodeList nList = doc.getElementsByTagName("dog");

      Node nNode = nList.item(0);
      Element eElement = (Element) nNode;
      d.setName(eElement.getAttribute("name"));
    } catch (ParserConfigurationException pce) {
      pce.printStackTrace();
    } catch (SAXException se) {
      se.printStackTrace();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

    return d;
  }
}
